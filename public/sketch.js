const w = 700;
const h = 700;

let scoreElem;
let score = 0;

let maxScoreElem;

function setup() {
    scoreElem = createDiv('Score: ' + score);
    scoreElem.position(20, 20);
    scoreElem.id = 'score';
    scoreElem.style('color', 'black');

    maxScoreElem = createDiv('Max Score: ');
    maxScoreElem.position(20, 50);
    maxScoreElem.id = 'maxScore';
    maxScoreElem.style('color', 'black');

    db.collection("scores").orderBy("score", "desc").limit(1).onSnapshot(function(snapshot) {
        maxScoreElem.html("Max Score: " + snapshot.docs[0].data().score);
    }, function(error) {
        //...
    });

    let c = createCanvas(w, h);
    let x = (windowWidth - width) / 2;
    let y = (windowHeight - height) / 2;
    c.position(x, y);
    frameRate(30);
}

function draw() {
    if (mouseTimeout > 0) {
        mouseTimeout--;
    }

    updateColumns();
    updateFlappy();

    background(0, 0, 0);

    drawColumns();
    drawFlappy();
}

const mouseTimeoutMax = 5;
let mouseTimeout = 0;

function mousePressed() {
    if (mouseTimeout === 0) {
        mouseTimeout = mouseTimeoutMax;

        flappyYd = flappyClickImpactYdd;
    }
}

const flappySize = 70;
const flappyX = 100;
let flappyY = 500;
let flappyYd = 0;

const flappyYdCap = 30;
const flappyClickImpactYdd = -15;

const gravityYdd = 1;


function updateFlappy() {
    if (((flappyY + flappySize / 2) < h && flappyYd >= 0)
        || ((flappyY - flappySize / 2) > 0 && flappyYd <= 0)) {
        if (flappyYd > flappyYdCap) {
            flappyYd = flappyYdCap;
        } else if (flappyYd < -flappyYdCap) {
            flappyYd = -flappyYdCap;
        }

        flappyY += flappyYd;
        flappyYd += gravityYdd;
    } else {
        flappyYd = 0;
    }

    if (intersectsAnyColumn(flappyX - flappySize / 2, flappyY - flappySize / 2, flappySize, flappySize)) {
        scoreElem.html("Game Over! Score: " + score);
        noLoop();
    }
}

function drawFlappy() {
    push();
    fill("blue");
    rect(flappyX-flappySize/2, flappyY-flappySize/2, flappySize, flappySize);
}

const scrollXd = -3.5;

let columnXs = [];
let columnOpeningYs = [];
let columnPassess = [];

const columnWidth = 50;
const columnOpeningHeight = 250;

const columnHalf = columnWidth / 2;
const columnOpeningHalf = columnOpeningHeight / 2;

function randomOpeningHeight() {
    return random(columnOpeningHeight / 2, (width - columnOpeningHeight / 2));
}

let columnCounter = 0;
const columnCounterMax = 100;

function updateColumns() {
    if (columnCounter <= 0) {
        columnCounter = columnCounterMax;
        columnXs.push(width + columnHalf);
        columnOpeningYs.push(randomOpeningHeight());
        columnPassess.push(false);
    }
    columnCounter--;

    columnXs.forEach(function (element, index, array) {
        array[index] = element + scrollXd;
    });

    let newPassIdx = columnXs.findIndex(function (value, index, array) {
        return value < flappyX && !columnPassess[index];
    });
    if (newPassIdx >= 0) {
        score++;
        scoreElem.html("Score: " + score);
        db.collection("scores").add({
            score: score
        });
        columnPassess[newPassIdx] = true;
    }

    if (columnXs.length > 0) {
        let firstX = columnXs[0];
        if (firstX < -columnHalf) {
            columnXs.shift();
            columnOpeningYs.shift();
            columnPassess.shift();
        }
    }
}

function intersectsAnyColumn(boxX, boxY, boxW, boxH) {
    return columnXs.map(function (x, index, array) {
        let openingY = columnOpeningYs[index];
        return intersectsColumn(x, openingY, boxX, boxY, boxW, boxH);
    }).reduce(function (prev, cur, curIdx, array) {
        return prev || cur;
    });
}

function intersectsColumn(x, openingY, boxX, boxY, boxW, boxH) {
    return checkIntersects(x - columnHalf, 0, columnWidth, openingY - columnOpeningHalf, boxX, boxY, boxW, boxH) ||
        checkIntersects(x - columnHalf, openingY + columnOpeningHalf, columnWidth, (height - openingY) - columnOpeningHalf, boxX, boxY, boxW, boxH);
}

function checkIntersects(x, y, w, h, x2, y2, w2, h2) {
    return x2 < x + w && x2 + w2 > x &&
           y2 < y + h && y2 + h2 > y;
}

function drawColumns() {
    push();
    fill("red");
    columnXs.forEach(function (x, index, array) {
        let openingY = columnOpeningYs[index];
        rect(x - columnHalf, 0, columnWidth, openingY - columnOpeningHalf);
        rect(x - columnHalf, openingY + columnOpeningHalf, columnWidth, (height - openingY) - columnOpeningHalf);
    });
    pop();
}
